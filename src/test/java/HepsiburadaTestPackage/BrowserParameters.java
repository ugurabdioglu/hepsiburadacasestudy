package HepsiburadaTestPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ugurabdioglu
 */
public class BrowserParameters {
    
    public String firefoxDriverPath = "/Users/ugurabdioglu/downloads/geckodriver";
    public String firefoxSetProperty = "webdriver.gecko.driver";
    
    public String chromeDriverPath = "/Users/ugurabdioglu/downloads/chromedriver";
    public String chromeSetProperty = "webdriver.chrome.driver";

    public String getFirefoxDriverPath() {
        return firefoxDriverPath;
    }

    public void setFirefoxDriverPath(String firefoxDriverPath) {
        this.firefoxDriverPath = firefoxDriverPath;
    }

    public String getFirefoxSetProperty() {
        return firefoxSetProperty;
    }

    public void setFirefoxSetProperty(String firefoxSetProperty) {
        this.firefoxSetProperty = firefoxSetProperty;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public void setChromeDriverPath(String chromeDriverPath) {
        this.chromeDriverPath = chromeDriverPath;
    }

    public String getChromeSetProperty() {
        return chromeSetProperty;
    }

    public void setChromeSetProperty(String chromeSetProperty) {
        this.chromeSetProperty = chromeSetProperty;
    }
    
}
