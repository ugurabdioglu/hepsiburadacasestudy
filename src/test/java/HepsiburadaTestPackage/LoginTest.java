/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HepsiburadaTestPackage;

import HepsiburadaTestPackage.BrowserParameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author ugurabdioglu
 */
public class LoginTest {

    public LoginTest() {
    }
    
    
    private WebDriver driver;
    private static WebElement element = null;
    BrowserParameters firefoxParameters = new BrowserParameters();

    @BeforeTest
    public void beforeLogin() {
        // For Mozilla Firefox Driver
        System.setProperty(firefoxParameters.getFirefoxSetProperty(), firefoxParameters.getFirefoxDriverPath());
        driver = new FirefoxDriver();

        // Open browser window and maximize it to full screen
        driver.manage().window().maximize();
    }

    @Test
    public void testToLogin() throws Exception {

        String usernameAreaXpath = "//*[@id=\"txtUserName\"]";

        // Go to the "Hepsiburada" login page
        driver.navigate().to("https://giris.hepsiburada.com");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(usernameAreaXpath)));

        element = driver.findElement(By.xpath(usernameAreaXpath));
        element.sendKeys("deneme");

    }

    @AfterTest
    public void afterLogin() {
        
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            System.out.println("Exception:" + e);
        }
        
        
        driver.quit();
    }
    
}
