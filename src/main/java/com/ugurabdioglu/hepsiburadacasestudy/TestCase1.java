/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ugurabdioglu.hepsiburadacasestudy;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author ugurabdioglu
 */
public class TestCase1 {

    public TestCase1() {
    }
    
    
    private static WebElement element = null;
    BrowserParameters firefoxParameters = new BrowserParameters();
    WebDriverWait wait;

    @BeforeTest
    public void beforeLogin(WebDriver driver) {
        // Open browser window and maximize it to full screen
        driver.manage().window().maximize();
    }

    @Test
    public void testToLogin(WebDriver driver) throws Exception {

        String usernameAreaXpath = "//*[@id=\"txtUserName\"]";
        String passwordAreaXpath = "//*[@id=\"txtPassword\"]";
        String loginButtonAreaXpath = "//*[@id=\"btnLogin\"]";
                
        waitHere(1000);

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(usernameAreaXpath)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(passwordAreaXpath)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(loginButtonAreaXpath)));
        

        element = driver.findElement(By.xpath(usernameAreaXpath));
        element.sendKeys("test12345678@test.test");
        
        element = driver.findElement(By.xpath(passwordAreaXpath));        
        element.sendKeys("Test1234");
        
        element = driver.findElement(By.xpath(loginButtonAreaXpath));        
        element.click(); 

        waitHere(2000);

    }
    
    @Test
    public void searchProduct(WebDriver driver) throws Exception {
        
        String searchBoxOldClassName = "desktopOldAutosuggestTheme-input";
        String searchButtonClassName = "SearchBoxOld-buttonContainer";
        String selectElementClassName = "carousel-lazy-item";
        
        waitHere(2000);
        
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(searchBoxOldClassName)));  
        
        element = driver.findElement(By.className(searchBoxOldClassName));
        element.sendKeys("emsan düdüklü tencere");
        
      
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(searchButtonClassName)));
        element = driver.findElement(By.className(searchButtonClassName));
        element.click();
        
        
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(selectElementClassName)));
        element = driver.findElement(By.className(selectElementClassName));
        element.click();     

        waitHere(1000);
    
        
        
    }
    
    @Test
    public void addBasket(WebDriver driver) throws Exception {
    
        String addBasketDefault = "//*[@id=\"addToCart\"]";      
        
        waitHere(1000);
    
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(addBasketDefault))); 
        element = driver.findElement(By.xpath(addBasketDefault));
        element.click();
        
        waitHere(1000);
        
    }
    
    @Test
    public void addBasketAnotherMarket(WebDriver driver) throws Exception {
    
        String addBasketAnotherCssSelecetor = ".marketplace-list > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(3) > div:nth-child(1) > form:nth-child(1) > button:nth-child(15)";      
        
        waitHere(1000);
    
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(addBasketAnotherCssSelecetor))); 
        element = driver.findElement(By.cssSelector(addBasketAnotherCssSelecetor));
        element.click();
       
        waitHere(1000);
        
    }
    
    @Test
    public void continueToShopping(WebDriver driver) throws Exception {
        String continueToShoppingCssSelector = "a.sc-AxjAm:nth-child(2)";
        
        waitHere(1000);
        
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(continueToShoppingCssSelector))); 
        element = driver.findElement(By.cssSelector(continueToShoppingCssSelector));
        element.click();
        
        waitHere(1000);
    
    }
   

    @Test
    public void showBasket(WebDriver driver) throws Exception {
        String showBasketCssSelector = "a.sc-AxjAm:nth-child(1)";
        
        waitHere(1000);
        
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(showBasketCssSelector))); 
        element = driver.findElement(By.cssSelector(showBasketCssSelector));
        element.click();
        
        waitHere(1000);
    
    }

    public void waitHere(int milisecond){
        try {
            Thread.sleep(milisecond);
        } catch (InterruptedException e) {
            System.out.println("Exception:" + e);
        }
    }
    
    

    @AfterTest
    public void afterTest(WebDriver driver) {
        
        waitHere(5000);
        
        //driver.close();
    }
    
}
