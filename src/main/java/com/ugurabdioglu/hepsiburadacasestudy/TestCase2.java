/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ugurabdioglu.hepsiburadacasestudy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author ugurabdioglu
 */
public class TestCase2 {
    
    
    private static WebElement element = null;
    BrowserParameters firefoxParameters = new BrowserParameters();
    WebDriverWait wait;
 
    @BeforeTest
    public void beforeTest(WebDriver driver) {
        // Open browser window and maximize it to full screen
        driver.manage().window().maximize();
    }
    
    @Test
    public void selectCategory(WebDriver driver){
        String categoryCssSelector = "li.sf-MenuItems-1-U3X:nth-child(9) > span:nth-child(1) > span:nth-child(1)";
        String subCategoryCssSelector = ".item-2124 > span:nth-child(1)";
        
        waitHere(1000);
        
        element = driver.findElement(By.cssSelector(categoryCssSelector));
        Actions action= new Actions(driver); 
        action.moveToElement(element).perform();
        
        waitHere(1000);

        element = driver.findElement(By.cssSelector(subCategoryCssSelector));
        element.click();
        
    }
    
    @Test
    public void selectProduct(WebDriver driver){
        String selectElementClassName = "carousel-lazy-item";
        
        waitHere(2000);
        
        wait = new WebDriverWait(driver, 10);    
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(selectElementClassName)));
        element = driver.findElement(By.className(selectElementClassName));
        element.click();     

        waitHere(1000);
    }
    
    
    public void waitHere(int milisecond){
        try {
            Thread.sleep(milisecond);
        } catch (InterruptedException e) {
            System.out.println("Exception:" + e);
        }
    }
    
    @AfterTest
    public void afterTest(WebDriver driver) {
        waitHere(5000);
        //driver.close();
    }
    
}
