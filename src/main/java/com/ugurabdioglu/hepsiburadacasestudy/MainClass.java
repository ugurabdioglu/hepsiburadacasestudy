/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ugurabdioglu.hepsiburadacasestudy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author ugurabdioglu
 */
public class MainClass {
    public static void main(String[] args) throws Exception {
        BrowserParameters firefoxParameters = new BrowserParameters();
        System.setProperty(firefoxParameters.getFirefoxSetProperty(), firefoxParameters.getFirefoxDriverPath());
        
        WebDriver driver = new FirefoxDriver();
        TestCase1 testCase1 = new TestCase1();
        OpenMainPage mainPage = new OpenMainPage();
        
        testCase1.beforeLogin(driver);
        mainPage.openMain(driver);
        mainPage.goLoginPage(driver);
        testCase1.testToLogin(driver);
        testCase1.searchProduct(driver);
        testCase1.addBasket(driver);
        testCase1.continueToShopping(driver);
        testCase1.searchProduct(driver);
        testCase1.addBasketAnotherMarket(driver);
        testCase1.showBasket(driver);
        testCase1.afterTest(driver);              
        
        WebDriver driver2 = new FirefoxDriver();        
        TestCase2 testCase2 = new TestCase2();

        mainPage.beforeLogin(driver2);
        mainPage.openMain(driver2);
        testCase2.selectCategory(driver2);
        testCase2.selectProduct(driver2);
        testCase1.addBasket(driver2);
        testCase1.showBasket(driver2);
        testCase1.afterTest(driver2);        
    }
}
