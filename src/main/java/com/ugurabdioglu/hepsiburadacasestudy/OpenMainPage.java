/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ugurabdioglu.hepsiburadacasestudy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author ugurabdioglu
 */
public class OpenMainPage {
    
    private static WebElement element = null;
    BrowserParameters firefoxParameters = new BrowserParameters();
    WebDriverWait wait;

    @BeforeTest
    public void beforeLogin(WebDriver driver) {
        // Open browser window and maximize it to full screen
        driver.manage().window().maximize();
    }

    @Test
    public void openMain(WebDriver driver) throws Exception {

        // Go to the "Hepsiburada" login page
        driver.navigate().to("https://www.hepsiburada.com");
        
        waitHere(1000);
        
    }
    
    @Test
    public void goLoginPage(WebDriver driver) throws Exception {
        
        String myAccountCssSelector = "#myAccount";
        String loginCssSelector = "#login";
        
        element = driver.findElement(By.cssSelector(myAccountCssSelector));
        Actions action= new Actions(driver); 
        action.moveToElement(element).perform();
        
        waitHere(1000);

        element = driver.findElement(By.cssSelector(loginCssSelector));
        element.click();
    }
    
        
    
    public void waitHere(int milisecond){
        try {
            Thread.sleep(milisecond);
        } catch (InterruptedException e) {
            System.out.println("Exception:" + e);
        }
    }
    
}
